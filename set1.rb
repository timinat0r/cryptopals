require 'byebug'

class CryptoPal
  # first challenge
  def hex_to_base64(str)
    Util.base64_encode Util.hex_encode(str)
  end

  # second challenge
  def fixed_xor(str1, str2)
    Util.hex_decode Util.hex_encode(str1).bytes.zip(Util.hex_encode(str2).bytes).map { |a,b| a ^ b }.pack('c*')
  end
end

class Util
  def self.hex_encode(buffer)
    Array(buffer).pack('H*')
  end

  def self.hex_decode(buffer)
    buffer.unpack('H*')
  end

  def self.base64_encode(buffer)
    Array(buffer).pack('m0')
  end
end

class Challenge3
  def run(str)
    # Go through every ASCII char and XOR a str.length long char string against the entire hex string
    # See if anything interesting shows up
    # Looks like 'X' is the character: "Cooking MC's like a pound of bacon"
    hex = [str].pack('H*')
    (('a'..'z').to_a + ('A'..'Z').to_a).to_a.reduce({}) do |hash, char|
      test = (char * hex.length).bytes.zip(hex.bytes).map { |a,b| a ^ b }.pack('c*')
      hash[char] = test
      hash
    end
  end
end

class Challenge4
  def find_single_xor(str)
    # Apply solution to every string in the file and compare to /usr/share/dict/words?
    # Gotta be a way to speed that up
  end
end
